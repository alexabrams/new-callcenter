import React, {Component} from 'react';
import Field from './Field';
import styles from './RoomNight.css';

class RoomNight extends Component {
    render() {
        return (
            <div className="room-nights">
                <h4>Room Night {this.props.index}:</h4>
                <Field name={"Stay Date"} style="inline-block" input={this.props.data.stayDate}/>
                <Field name={"Rate"} style="inline-block" input={this.props.data.rate.amount}/>
                <Field name={"Type"} style="inline-block" input={this.props.data.type}/>
            </div>
        );
    }
}

export default RoomNight;