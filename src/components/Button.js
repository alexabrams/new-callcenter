import React, {Component} from 'react';

import styles from './Button.css';

class Button extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let bigClass = "button " + (this.props.style ? this.props.style : "");

        return (
            <button onClick={this.props.onClick} className={bigClass}>
                {this.props.children}
            </button>
        );
    }
}

export default Button;