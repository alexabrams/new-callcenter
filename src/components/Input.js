import React, {Component} from 'react';

import styles from './Input.css';

class Input extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            value : this.props.input ? this.props.input : '',
        }
    }

    componentDidUpdate() {
        let val = this.props.input ? this.props.input : "";
        if (this.state.value != val) {
            this.setState( {value : val ? val : ""});
        }
    }

    render() {
        let bigClass = "input " + (this.props.style ? this.props.style : "");

        return (
            <input id={this.props.id} type="text" value={this.state.value} className={bigClass} placeholder={this.props.placeholder} onChange={this.props.onChange}>
            </input>
        );
    }
}

export default Input;