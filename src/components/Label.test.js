import React from 'react';
import Label from './Label';
import renderer from 'react-test-renderer';

test('Inputs is displayed', () => {
    const component = renderer.create(
        <Label>PassKey</Label>,
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();

    tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});