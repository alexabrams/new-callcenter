import React, {Component} from 'react';
import Label from './Label';
import Input from './Input';

import styles from './Field.css';

class Field extends Component {
    constructor(props) {
        super(props);
    }

    render() {

        let div = this.props.break ?
        (
            <div className="field">
                <Label name={this.props.name}/><br/>
                <Input input={this.props.input} onChange={this.props.onChange} />
            </div>
        ) :
        (
            <div className="field">
                <Label name={this.props.name}/>
                <Input input={this.props.input} onChange={this.props.onChange} />
            </div>
        )

        return (
            div
        );
    }
}

export default Field;