import React, { Component } from 'react';

import styles from './Input.css';

class DefaultInput extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    let bigClass = "input " + (this.props.style ? this.props.style : "");

    return (
      <input id={this.props.id} value={this.props.input} className={bigClass} placeholder={this.props.placeholder}
        onChange={this.props.onChange}>
      </input>
    );
  }
}

export default DefaultInput;