import React, {Component} from 'react';

import styles from './Label.css';

class Label extends Component {
    render() {
        return (
            <span className="label">
        {this.props.name}:
      </span>
        );
    }
}

export default Label;