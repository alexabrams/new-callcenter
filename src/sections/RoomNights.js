import React, {Component} from 'react';
import RoomNight from './../components/RoomNight';
import styles from './../components/RoomNight.css';

class RoomNights extends Component {
  render() {
    return this.props.data.map(function (roomNight, ind) {
      return (
        <div className="room-night-container">
          <RoomNight data={roomNight} index={ind + 1}/>
        </div>
      )
    });
  }
}

export default RoomNights;