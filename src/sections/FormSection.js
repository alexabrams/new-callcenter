import React, {Component} from 'react';
import Input from './../components/Input';
import Label from './../components/Label';

class FormSection extends Component {
    render() {
        return (
            <div>
                <Label props={"Street"}/>
                <Input props={this.props.data.primaryGuest.address.line1}/><br/>
                <Label props={"City"}/>
                <Input props={this.props.data.primaryGuest.address.city}/><br/>
                <Label props={"State"}/>
                <Input props={this.props.data.primaryGuest.address.state}/><br/>
                <Label props={"Zipcode"}/>
                <Input props={this.props.data.primaryGuest.address.postalCode}/><br/>
                <Label props={"Country"}/>
                <Input props={this.props.data.primaryGuest.address.country.value}/>
            </div>
        )
    }
}

export default FormSection;