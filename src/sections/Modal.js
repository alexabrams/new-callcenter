import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';

import styles from './Modal.css';

const rootPortal = document.getElementById('portalRoot');

class Modal extends React.Component {
    render() {
        if(!this.props.show) {
            return null;
        }

        const backdropStyle = {
            position: 'fixed',
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
            backgroundColor: 'rgba(0,0,0,0.3)',
            padding: 50
        };

        const modalStyle = {
            backgroundColor: '#fff',
            borderRadius: 5,
            maxWidth: 500,
            minHeight: 300,
            margin: '0 auto',
            padding: 0
        };

        return ReactDOM.createPortal(
            <div className="backdrop" style={backdropStyle}>

                <div className="modal" style={modalStyle}>
                    <header>
                        <span>{this.props.header}</span>
                    </header>
                    {this.props.children}
                    <div className="footer">
                        <button onClick={this.props.onClose} type="button">
                            Close
                        </button>
                    </div>
                </div>
            </div>,
            rootPortal
        );
    }
}

Modal.propTypes = {
    onClose: PropTypes.func.isRequired,
    show: PropTypes.bool,
    children: PropTypes.node
};

export default Modal;