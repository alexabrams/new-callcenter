import React, {Component} from 'react';
import axios from 'axios';
import Header from "./Header";
import Button from '../components/Button';
import DefaultInput from '../components/DefaultInput';

class LogInForm extends Component {
    state = {
        userName: '',
        password: '',
        validCredentials: '',
    };

    handleSubmit = (event) => {
        event.preventDefault();
        axios.get(`https://passkey-authentication-service-development.us-east-1.lb.cvent.com/ci/passkey-authentication/v1/tokens/provided-config-token?username=${this.state.userName}&password=${this.state.password}&accessTokenTtl=60000&refreshTokenTtl=-1`,
            {
                headers: {'Authorization': `API_KEY 83d13535775b665c46ff4b563a0c3f31`}
            })
            .then(resp => {
                axios.get(`https://passkey-authentication-service-development.us-east-1.lb.cvent.com/ci/passkey-authentication/v1/tokens/details?accessToken=${resp.data.accessToken}`,
                    {
                        headers: {'Authorization': `API_KEY 83d13535775b665c46ff4b563a0c3f31`}
                    })
                    .then(user => {
                        this.props.onSubmit(resp.data, user.data);
                        this.setState({userName: ''});
                        this.setState({password: ''});
                    });
            })
            .catch(error => {
                this.setState({validCredentials: 'invalid'})
            });
    };

    render() {
        return (
            <div>
                <Header/>
                <form style={{textAlign: 'center', marginTop: '300px', }} onSubmit={this.handleSubmit}>
                    <DefaultInput type="text"
                           value={this.state.userName}
                           onChange={(event) => this.setState({userName: event.target.value})}
                           ref={(input) => this.userNameInput = input}
                           placeholder="Enter User Name" required/>
                    <br/>
                    <div style={{marginTop: '10px'}}>
                    <DefaultInput type="text"
                           value={this.state.password}
                           onChange={(event) => this.setState({password: event.target.value})}
                           ref={(input) => this.passwordInput = input}
                           placeholder="Enter Password" required style={{marginTop: '10px'}}/>
                    </div>
                    <br/>
                    <Button type="submit" style={{marginTop: '25px'}}>LogIn</Button>
                    {this.state.validCredentials ? <div>
                        <p>Please Enter Valid Credentials</p>
                    </div> : <br/>}
                </form>
            </div>
        );
    }
}

export default LogInForm;