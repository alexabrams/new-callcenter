import React, {Component} from 'react';
import Field from './../components/Field';
import Divider from './Divider';

class Status extends Component {
    render() {
        return (
            <Divider style="generic">
                <h3 className="section-header">Status</h3>
                <Field name={"Attendee Code"} input={this.props.data != null ? this.props.data.attendeeCode : ""} onChange={this.props.onChange}/>
                <Field name={"Arrival Date"} input={this.props.data.arrivalDate} onChange={this.props.onChange}/>
                <Field name={"Departure Date"} input={this.props.data.departureDate} onChange={this.props.onChange}/>
                <Field name={"Status"} input={this.props.data.status[0]} onChange={this.props.onChange}/>
            </Divider>
        )
    }
}

export default Status;