import React, {Component} from 'react';
import Field from './../components/Field';
import Divider from './Divider';

class PayInfo extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Divider style="super">
        <Divider style={"big-div"}>
          <h3 className="section-header">Pay Info</h3>
          <Field name = { "Pay Method" } input = {this.props.data.primaryGuest.payInfo != null ? this.props.data.primaryGuest.payInfo.payMethod : ""} onChange={this.props.onChange} />
          <Field name = { "Cardholder Name" } input = {this.props.data.primaryGuest.payInfo != null ? this.props.data.primaryGuest.payInfo.cardholderName : ""} onChange={this.props.onChange} />
          <Field name = { "Phone" } input = {this.props.data.primaryGuest.payInfo != null ? this.props.data.primaryGuest.payInfo.phone : ""} onChange={this.props.onChange} />
          <Field name = { "Card Number" } input = {this.props.data.primaryGuest.payInfo != null? this.props.data.primaryGuest.payInfo.cardNumber:""} onChange={this.props.onChange} />
          <Field name = { "Type" } input = {this.props.data.primaryGuest.payInfo != null? this.props.data.primaryGuest.payInfo.creditCard.type:""} onChange={this.props.onChange} />
        </Divider>
        <Divider style={"big-div"}>
          <h3 className="section-header">&nbsp;</h3>
          <Field name = { "Exp. Date" } input = {this.props.data.primaryGuest.payInfo != null? this.props.data.primaryGuest.payInfo.expDate:""} onChange={this.props.onChange} />
          <Field name = { "CVV" } input = {this.props.data.primaryGuest.payInfo != null? this.props.data.primaryGuest.payInfo.cvv:""} onChange={this.props.onChange} />
        </Divider>
      </Divider>
    );
  }
}

export default PayInfo;

//<Address data={this.props.data != null ? this.props.data.primaryGuest.payInfo.address : {}} />
//<CreditCard data={this.props.data != null ? this.props.data.primaryGuest.payInfo.creditCard : {}} />