import React from 'react';
import Block from './Block';

const BlockList = (props) => {
    return (
        <div>
            {props.blocks.map((block, ind) => <Block key={block.id} index={ind} {...block} toggleModal={props.toggleModal}/>)}
        </div>
    );
};

export default BlockList;