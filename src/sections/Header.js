import React, {Component} from 'react';
import logo from "../Passkey-logo.png";

import styles from './Header.css';

class Header extends Component {
    render() {
        return (
            <div className="header">
                <img src={logo} alt="logo"/>
                <h3> {this.props.header} </h3>
            </div>
        );
    }
}

export default Header;