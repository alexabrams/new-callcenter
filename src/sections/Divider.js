import React, {Component} from 'react';

import styles from './Divider.css';

class Divider extends Component {
    render() {
        let bigClass = "divider " + (this.props.style ? this.props.style : "");

        return (
            <div className={bigClass}>
                {this.props.children}
            </div>
        );
    }
}

export default Divider;