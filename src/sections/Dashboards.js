import React, {Component} from 'react';
import axios from 'axios';
import Divider from "./Divider";
import Button from "../components/Button";
import Label from './../components/Label';
import BlockList from '../sections/BlockList';
import DefaultInput from '../components/DefaultInput';
import Modal from './Modal';

class Form extends Component {
    state = {
        eventID: '',
        Authorization: `bearer ${this.props.authorizationToken}`,
    };

    header = {
        // Authorization: `bearer ${this.props.authorizationToken}`
        Authorization: `bearer 971e966735aba1ad1a78a63dfda59d08`
    };

    handleSubmit = (event) => {
        event.preventDefault();
        axios.get(`https://passkey-event-service-development.us-east-1.lb.cvent.com/ci/passkey-event/v1/events/${this.state.eventID}/webinfo`, {headers: this.header})
            .then(resp => {
                this.props.onSubmit(resp.data);
                this.setState({eventID: ''});
            });
    };

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <Divider style={"inline-block left"}>
                    <Label name={"Event ID"}/>
                    <DefaultInput type="text"
                                  value={this.state.eventID}
                                  onChange={(event) => this.setState({eventID: event.target.value})}
                                  ref={(input) => this.userNameInput = input}
                                  placeholder="Enter Event ID" required/>
                    <Button type="submit">Add Event</Button>
                </Divider>
            </form>
        );
    }
}

class Dashboards extends Component {
    state = {
        blocks: [],
        authorizationToken: this.props.authorizationToken,
        dashboardsNavigation: 'true',
        isOpen: false,
        event: [],
        webInfoSummary: [],
        eventInfo: []
    };

    header = {
        // Authorization: `bearer ${this.props.authorizationToken}`
        Authorization: `bearer 971e966735aba1ad1a78a63dfda59d08`
    };

    addNewBlock = (webInfo) => {
        this.setState(prevState => ({
            blocks: prevState.blocks.concat(webInfo)
        }))
    };

    toggleModal = () => {
        axios.get(`https://passkey-event-service-development.us-east-1.lb.cvent.com/ci/passkey-event/v1/events?identifier=${this.state.blocks[0].eventId}`, {headers: this.header})
            .then(resp => {
                this.setState({
                    isOpen: !this.state.isOpen,
                    event: resp.data[0],
                    webInfoSummary: resp.data[0].webInfoSummary,
                    eventInfo: resp.data[0].eventInfo
                });
            });
    };

    render() {
        return (
            <div>
                <div>
                    <Divider style="search-header">
                        <Form authorizationToken={this.state.authorizationToken} onSubmit={this.addNewBlock}
                              toggleModal={this.toggleModal}/>
                        <div style={{marginTop: '20px'}}>
                            <span>City Wide: {this.props.reportRoomNights} Room Nigts For Year</span><br/>
                            <span>City Wide: {this.props.reportReservations} Reservations For Year</span>
                        </div>
                        <div style={{marginTop: '-60px'}}>
                        <Button onClick={this.props.navigateToCallCenter} style={"inline-block right"}>Call
                            Center</Button>
                        </div>
                    </Divider>
                    <div className="line-div"></div>
                    <BlockList blocks={this.state.blocks} toggleModal={this.toggleModal}/>
                    <Modal show={this.state.isOpen} header={"User Information"}
                           onClose={this.toggleModal}>
                    <span>
                        Event Name: {this.state.event.name} <br/><br/>
                        Event ID: {this.state.event.id} <br/>
                        Event Location: {this.state.event.location} <br/>
                        Event Status: {this.state.eventInfo.status} <br/>
                        Event City Wide: {this.state.eventInfo.isCityWide} <br/>
                        Event Type ID: {this.state.eventInfo.typeId} <br/><br/>
                        Event Res Access: {this.state.webInfoSummary.isResAccess}
                    </span>
                    </Modal>
                    <div className="line-div"></div>
                </div>
            </div>
        );
    }
}

export default Dashboards;