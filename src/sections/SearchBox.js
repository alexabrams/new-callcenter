import React, {Component} from 'react';
import Divider from './Divider';
import Label from './../components/Label';
import Button from './../components/Button';

class SearchBox extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Divider style={"inline-block left"}>
                <Label name={"Confirmation Number"}/>
                <input class="input" id="confirmNum" placeholder="Enter Confirmation Number" /> 
                <Button id="findRes" onClick={this.props.onClick} >Find Reservation</Button>
            </Divider>
        );
    }
}

export default SearchBox;