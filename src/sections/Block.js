import React from 'react';
import Divider from "./Divider";
import Button from "../components/Button";

const Block = (props) => {
    return (
        <div style={{display: 'inline-block', textAlign: 'left'}}>
            <div className="surrounding-div">
                <h3></h3>
                <Divider style={"inline-block"}>
                    <div>Hotel ID: {props.hotelId}</div>
                    <div><Button onClick={props.toggleModal} style={{marginTop: '25px'}}>Event ID: {props.eventId}</Button></div>
                    <div>Start Date: {props.startDate}</div>
                    <div>End Date: {props.endDate}</div>
                    <div>Cutoff Date: {props.cutoffDate}</div>
                    <div>Shutoff Date: {props.shutOffDate}</div>
                    <div>Contracted Start Date: {props.contractedStartDate}</div>
                    <div>Contracted End Date: {props.contractedEndDate}</div>
                </Divider>
            </div>
        </div>
    )
};

export default Block;