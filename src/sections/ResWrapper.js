import React, { Component } from 'react';
import Address from './Address';
import Guest from './Guest';
import Status from './Status';
import RoomNights from './RoomNights';
import PayInfo from './PayInfo';
import SearchBox from './SearchBox';
import Button from '../components/Button';
import Divider from './Divider';
import axios from 'axios';
import styles from './ResWrapper.css';

class ResWrapper extends Component {

    constructor(props) {
        super(props);
        this.state = {
            "attendeeCode": "",
            "arrivalDate": "",
            "departureDate": "",
            "status": [
                ""
            ],
            "totalCharges": {
                "amount": 200,
                "currency": "USD"
            },
            "room": {
                "blockId": 529857252,
                "roomNights": [
                    {
                        "stayDate": "",
                        "guests": [
                            {
                                "id": 0,
                                "firstName": "",
                                "lastName": "",
                                "middleName": "",
                                "title": "",
                                "position": "",
                                "pkOrganization": "",
                                "faxNumber1": "",
                                "email": "",
                                "phone": "",
                                "age": 18,
                                "address": {
                                    "line1": "",
                                    "city": "",
                                    "state": "",
                                    "postalCode": "",
                                    "country": {
                                        "isoCode": "",
                                        "value": ""
                                    }
                                },
                                "locale": "EN_US",
                                "payInfo": {
                                    "payMethod": "0",
                                    "cardholderName": "",
                                    "address": {
                                        "line1": "",
                                        "city": "",
                                        "state": "",
                                        "postalCode": "",
                                        "country": {
                                            "isoCode": "US",
                                            "value": "US"
                                        }
                                    },
                                    "phone": "",
                                    "creditCard": {
                                        "cardNumber": "4111111111111111",
                                        "expDate": "",
                                        "cvv": 354,
                                        "type": "VISA"
                                    }
                                },
                                "isChild": false,
                                "isPrimary": true
                            }
                        ],
                        "rate": {
                            "amount": "100",
                            "currency": "USD"
                        },
                        "type": ""
                    }
                ]
            },
            "lastModified": "",
            "numberOfGuests": 1,
            "numberOfChildren": 0,
            "custom1": "",
            "custom2": "",
            "custom3": "",
            "custom4": "",
            "custom5": "",
            "primaryGuest": {
                "id": 0,
                "firstName": "",
                "lastName": "",
                "title": "",
                "position": "",
                "pkOrganization": "",
                "email": "",
                "phone": "",
                "age": 18,
                "address": {
                    "line1": "",
                    "city": "",
                    "state": "",
                    "postalCode": "",
                    "country": {
                        "value": ""
                    }
                },
                "locale": "en_US"
            },
            authorizationToken: this.props.authorizationToken,
            dashboardsNavigation: ''
        };
        this.findRes = this.findRes.bind(this);
        this.createRes = this.createRes.bind(this);
        this.handleChange = this.handleChange.bind(this);
    };

    handleChange(e) {
        const self = this;
        const id = e.target.id.toString();
        if (id === "confirmNum") {
            return;
        }
        console.log("target id=" + id);
        let room = this.state.room;
        let roomNight = this.state.room.roomNights[0];
        let guest = this.state.room.roomNights[0].guests[0];
        let primaryGuest = this.state.primaryGuest;
        let primaryPayInfoAddress = this.state.primaryGuest.payInfo.address;
        let primaryAddress = this.state.primaryGuest.address;
        let address = this.state.room.roomNights[0].guests[0].address;
        let payInfo = this.state.room.roomNights[0].guests[0].payInfo;
        let creditCard = this.state.room.roomNights[0].guests[0].payInfo.creditCard;
        if ( id === "firstName") {
            guest.firstName = e.target.value;
            self.setState({guest});
            primaryGuest.firstName = e.target.value;
            self.setState({primaryGuest});
        } else if (id === "lastName") {
            guest.lastName = e.target.value;
            self.setState({guest});
            primaryGuest.lastName = e.target.value;
            self.setState({primaryGuest});
        } else if (id === "email") {
            guest.email = e.target.value;
            self.setState({guest});
            primaryGuest.email = e.target.value;
            self.setState({primaryGuest});
        } else if (id === "position") {
            guest.position = e.target.value;
            self.setState({guest});
            primaryGuest.position = e.target.value;
            self.setState({primaryGuest});
        } else if (id === "pkOrganization") {
            guest.pkOrganization = e.target.value;
            self.setState({guest});
            primaryGuest.pkOrganization = e.target.value;
            self.setState({primaryGuest});
        } else if (id === "isChild") {
            guest.isChild = e.target.value == "on"? true : false;
            self.setState({guest});
            primaryGuest.isChild = e.target.value;
            self.setState({primaryGuest});
        } else if (id === "isPrimary") {
            guest.isPrimary = e.target.value == "on" ? true : false;
            self.setState({guest});
            primaryGuest.isPrimary = e.target.value;
            self.setState({primaryGuest});
        } else if (id === "cardHolderName") {
            payInfo.cardholderName = e.target.value;
            self.setState({payInfo});
            if (this.state.primaryGuest != null) {
                let primaryPayInfo = this.state.primaryGuest.payInfo;
                primaryPayInfo.cardholderName = e.target.value;
                self.setState({primaryPayInfo});
            }
        } else if (id === "phone") {
            payInfo.phone = e.target.value;
            self.setState({payInfo});
            if (this.state.primaryGuest != null) {
                let primaryPayInfo = this.state.primaryGuest.payInfo;
                primaryPayInfo.phone = e.target.value;
                self.setState({primaryPayInfo});
            }
        } else if (id === "expDate") {
            creditCard.expDate = e.target.value;
            self.setState({creditCard});
            if (this.state.primaryGuest != null) {
                let primaryCreditCard = this.state.primaryGuest.payInfo.creditCard;
                primaryCreditCard.expDate = e.target.value;
                self.setState({primaryCreditCard});
            }
        } else if (id === "cvv") {
            creditCard.cvv = e.target.value;
            self.setState({creditCard});
            if (this.state.primaryGuest != null) {
                let primaryCreditCard = this.state.primaryGuest.payInfo.creditCard;
                primaryCreditCard.cvv = e.target.value;
                self.setState({primaryCreditCard});
            }
        } else if (id === "cardNumber") {
            creditCard.cardNumber = e.target.value;
            self.setState({creditCard});
            if (this.state.primaryGuest != null) {
                let primaryCreditCard = this.state.primaryGuest.payInfo.creditCard;
                primaryCreditCard.cardNumber = e.target.value;
                self.setState({primaryCreditCard});
            }
        } else if (id === "attendeeCode") {
            this.setState({"attendeeCode": e.target.value});
        } else if (id === "arrivalDate") {
            this.setState({"arrivalDate": e.target.value});
        } else if (id === "departureDate") {
            this.setState({"departureDate": e.target.value});
        } else if (id === "totalCharges") {
            let totalCharges = this.state.totalCharges;
            totalCharges.amount = e.target.value;
            totalCharges.currency = "USD";
            this.setState(totalCharges);
        } else if (id === "status") {
            let status = this.state.status;
            status[0] = e.target.value;
            this.setState(status);
        } else if (id === "blockId") {
            room.blockId = e.target.value;
            this.setState({room});
        } else if (id === "stayDate") {
            roomNight.stayDate = e.target.value;
            this.setState({roomNight});
        } else if (id === "rate") {
            roomNight.rate.amount = e.target.value;
            roomNight.rate.currency = "USD";
            this.setState({roomNight});
        } else if (id === "statusType") {
            roomNight.type = e.target.value;
            this.setState({roomNight});
        } else if (id == "line1") {
            address.line1 = e.target.value;
            payInfo.address.line1 = e.target.value;
            this.setState({address});
            this.setState({payInfo});
            primaryAddress.line1 = e.target.value;
            primaryPayInfoAddress.line1 = e.target.value;
            this.setState({primaryAddress});
            this.setState({primaryPayInfoAddress});
        } else if (id === "city") {
            address.city = e.target.value;
            this.setState({address});
            payInfo.address.city = e.target.value;
            this.setState({payInfo});
            primaryAddress.city = e.target.value;
            primaryPayInfoAddress.city = e.target.value;
            this.setState({primaryAddress});
            this.setState({primaryPayInfoAddress});
        } else if (id === "state") {
            address.state = e.target.value;
            this.setState({address});
            payInfo.address.state = e.target.value;
            this.setState({payInfo});
            primaryAddress.state = e.target.value;
            primaryPayInfoAddress.state = e.target.value;
            this.setState({primaryAddress});
            this.setState({primaryPayInfoAddress});
        } else if (id === "postalCode") {
            address.postalCode = e.target.value;
            this.setState({address});
            payInfo.address.postalCode = e.target.value;
            this.setState({payInfo});
            primaryAddress.postalCode = e.target.value;
            primaryPayInfoAddress.postalCode = e.target.value;
            this.setState({primaryAddress});
            this.setState({primaryPayInfoAddress});
        } else if (id === "country") {
            address.country.value = e.target.value;
            address.country.isoCode = e.target.value;
            this.setState({address});
            payInfo.address.country.value = e.target.value;
            payInfo.address.country.isoCode = e.target.value;
            this.setState({payInfo});
            primaryAddress.country.value = e.target.value;
            primaryAddress.country.isocode = e.target.value;
            primaryPayInfoAddress.country.value = e.target.value;
            primaryPayInfoAddress.country.isoCode = e.target.value;
            this.setState({primaryAddress});
            this.setState({primaryPayInfoAddress});
        }
        console.log(this.state);
    }


  findRes() {
    let headers = {
      'Content-Type': 'application/json',
        'Authorization': `bearer ${this.props.authorizationToken}`
        // 'Authorization': 'API_KEY 289b62a34ee9745b5d95095f6d69d02c'
    };
    let confirmNum = document.getElementById("confirmNum").value;
    console.log(confirmNum);
    const self = this;
    axios.get("https://passkey-reservation-service-development.us-east-1.lb.cvent.com/ci/passkey-reservation/v1/reservations/" + confirmNum
      , { headers: headers })
      .then(function (res) {
        console.log(res);
        self.setState(res.data);
      });
  }
    findRes() {
        let headers = {
            'Content-Type': 'application/json',
            //'Authorization': `bearer ${this.props.authorizationToken}`
            'Authorization': 'API_KEY 289b62a34ee9745b5d95095f6d69d02c'
        };
        let confirmNum = document.getElementById("confirmNum").value;
        console.log(confirmNum);
        const self = this;
        axios.get("https://passkey-reservation-service-development.us-east-1.lb.cvent.com/ci/passkey-reservation/v1/reservations/" + confirmNum
            , { headers: headers })
            .then(function (res) {
                console.log(res);
                self.setState(res.data);
            });
    }

    createRes() {
        let headers = {
            'Content-Type': 'application/json',
            'Authorization': `bearer ${this.props.authorizationToken}`
            // 'Authorization': 'API_KEY 289b62a34ee9745b5d95095f6d69d02c'
        };
    }

    render() {

        let roomNights = this.state.room.roomNights[0].stayDate ?
            (
                <div>
                    <div className="line-div"></div>
                    <div className="room-nights-container">
                        <RoomNights data={this.state.room.roomNights} />
                    </div>
                    <div className="line-div"></div>
                </div>
            ) : "";

        return (
            <div>
                <div>
                    <Divider style="search-header">
                        <SearchBox onClick={this.findRes} onChange={this.handleChange} />
                        <Button onClick={this.props.navigateToDashboards} style={"inline-block right"} >Event
                            Dashboard</Button>
                    </Divider>
                    <div className="line-div"></div>
                    <div className="surrounding-div">
                        <Guest data={this.state} onChange={this.handleChange} />
                        <Address data={this.state} onChange={this.handleChange} />
                        <Status data={this.state} onChange={this.handleChange} />
                        <PayInfo data={this.state} onChange={this.handleChange} />
                    </div>
                    {roomNights}
                </div>
            </div>
        );
    }
}

export default ResWrapper;

//<RoomNights data={this.state.room.roomNights} />