import React, {Component} from 'react';
import Field from './../components/Field';
import Divider from './Divider';

import styles from './Guest.css';

class GuestRow extends Component {
    render() {
        return (
            <Divider style="generic">
                <h3 className="section-header">Guest Info</h3>
                <Field name={"First Name"} input={this.props.data.primaryGuest.firstName} onChange={this.props.onChange}/>
                <Field name={"Last Name"} input={this.props.data.primaryGuest.lastName} onChange={this.props.onChange}/>
                <Field name={"Email"} input={this.props.data.primaryGuest.email} onChange={this.props.onChange}/>
                <Field name={"Organization"} input={this.props.data.primaryGuest.pkOrganization} onChange={this.props.onChange}/>
                <Field name={"Position"} input={this.props.data.primaryGuest.position} onChange={this.props.onChange}/>
            </Divider>
        )
    }
}

export default GuestRow;