import React, {Component} from 'react';
import Field from './../components/Field';
import Divider from './Divider';

class Address extends Component {
    render() {
        return (
            <Divider style="generic">
                <h3 className="section-header">Address</h3>
                <Field name={"Street"} input={this.props.data.primaryGuest.address.line1} onChange={this.props.onChange}/>
                <Field name={"City"} input={this.props.data.primaryGuest.address.city} onChange={this.props.onChange}/>
                <Field name={"State"} input={this.props.data.primaryGuest.address.state} onChange={this.props.onChange}/>
                <Field name={"Zipcode"} input={this.props.data.primaryGuest.address.postalCode} onChange={this.props.onChange}/>
                <Field name={"Country"} input={this.props.data.primaryGuest.address.country.value} onChange={this.props.onChange}/>
            </Divider>
        )
    }
}

export default Address;