import React, {Component} from 'react';
import LogInForm from "./sections/LogInForm";
import Header from './sections/Header';
import ResWrapper from './sections/ResWrapper';
import Divider from './sections/Divider';
import userIcon from './new-user-icon.png';
import Modal from './sections/Modal';
import styles from './Default.css';
import appStyles from './App.css';
import Dashboards from './sections/Dashboards';
import axios from "axios/index";

class App extends Component {
    constructor(props) {
        super(props);
        this.navigateToCallCenter = this.navigateToCallCenter.bind(this);
    }

    state = {
        authorizationToken: '',
        dashboardsNavigation: '',
        user: [],
        isOpen: false,
        reportRoomNights: [],
        reportReservations: []
    };

    header = {
        // Authorization: `bearer ${this.props.authorizationToken}`
        Authorization: `bearer 971e966735aba1ad1a78a63dfda59d08`
    };

    toggleModal = () => {
        this.setState({
            isOpen: !this.state.isOpen
        });
    };

    getAccessToken = (accessToke, userData) => {
        if (accessToke) {
            this.setState(() => ({
                authorizationToken: accessToke.accessToken,
                user: userData
            }));
        }
    };

    navigateToCallCenter = () => {
        this.setState(prevState => ({
            authorizationToken: prevState.authorizationToken,
            dashboardsNavigation: ''
        }));
    };

    navigateToDashboards = () => {
        axios.get(`https://passkey-reporting-service-development.us-east-1.lb.cvent.com/ci/passkey-reporting/v1/bookings?startDate=2018-01-01&endDate=2018-06-08&eventCategory=CW`, {headers: this.header})
            .then(report => {
                this.setState(prevState => ({
                    authorizationToken: prevState.authorizationToken,
                    dashboardsNavigation: 'true',
                    user: prevState.user,
                    isOpen: prevState.isOpen,
                    reportRoomNights: report.data.roomNights,
                    reportReservations: report.data.reservations
                }));
            });
    };

    render() {

        let div;
        if (this.state.authorizationToken) {
            if (this.state.dashboardsNavigation === '') {
                div = (
                    <div>
                        <Divider style="search-header">
                            <Header header={"Cvent Passkey Reservation Center"}/>
                            <p style={{display: 'inline-block', float: 'right', marginTop: '-90px'}}>
                                User Type: {this.state.user.authorization.metadata.participantType}
                                <br/>
                                User Name: {this.state.user.authorization.metadata.username}
                                <br/>
                                User ID: {this.state.user.authorization.metadata.userId}
                            </p>
                            <img src={userIcon} alt='user icon'
                                 style={{
                                     width: '50px',
                                     display: 'inline-block',
                                     float: 'right',
                                     cursor: 'pointer',
                                     marginTop: '-80px',
                                     marginRight: '210px'
                                 }}
                                 onClick={this.toggleModal}/>
                            <Modal show={this.state.isOpen} header={"User Information"}
                                   onClose={this.toggleModal}><span>
                                User Type: {this.state.user.authorization.metadata.participantType}
                                <br/>
                                User Name: {this.state.user.authorization.metadata.username}
                                <br/>
                                User ID: {this.state.user.authorization.metadata.userId}
                                <br/>
                                Sister Properties: {this.state.user.authorization.metadata.sisterProperties}
                                <br/>
                                API User: {this.state.user.authorization.metadata.apiUser}
                                <br/>
                                Participant ID: {this.state.user.authorization.metadata.participantId}
                                <br/>
                                Admin User: {this.state.user.authorization.metadata.adminUser}
                                <br/>
                                Has Sister Properties: {this.state.user.authorization.metadata.hasSisterProperties}
                                <br/>
                                Roles: {this.state.user.authorization.grantedAuthorizations.roles}
                                <br/>
                                </span>
                            </Modal>
                        </Divider>
                        <div className="line-div"></div>
                        <ResWrapper authorizationToken={this.state.authorizationToken}
                                    navigateToDashboards={this.navigateToDashboards}/>
                    </div>
                )
            } else {
                div = (
                    <div>
                        <Divider style="search-header">
                            <Header header={"Cvent Passkey Event Dashboard"}/>
                            <p style={{display: 'inline-block', float: 'right', marginTop: '-90px'}}>
                                User Type: {this.state.user.authorization.metadata.participantType}
                                <br/>
                                User Name: {this.state.user.authorization.metadata.username}
                                <br/>
                                User ID: {this.state.user.authorization.metadata.userId}
                            </p>
                            <img src={userIcon} alt='user icon'
                                 style={{
                                     width: '50px',
                                     display: 'inline-block',
                                     float: 'right',
                                     cursor: 'pointer',
                                     marginTop: '-80px',
                                     marginRight: '210px'
                                 }}
                                 onClick={this.toggleModal}/>
                            <Modal show={this.state.isOpen} header={"User Information"}
                                   onClose={this.toggleModal}><span>
                                User Type: {this.state.user.authorization.metadata.participantType}
                                <br/>
                                User Name: {this.state.user.authorization.metadata.username}
                                <br/>
                                User ID: {this.state.user.authorization.metadata.userId}
                                <br/>
                                Sister Properties: {this.state.user.authorization.metadata.sisterProperties}
                                <br/>
                                API User: {this.state.user.authorization.metadata.apiUser}
                                <br/>
                                Participant ID: {this.state.user.authorization.metadata.participantId}
                                <br/>
                                Admin User: {this.state.user.authorization.metadata.adminUser}
                                <br/>
                                Has Sister Properties: {this.state.user.authorization.metadata.hasSisterProperties}
                                <br/>
                                Roles: {this.state.user.authorization.grantedAuthorizations.roles}
                                <br/>
                                </span>
                            </Modal>
                        </Divider>
                        <div className="line-div"></div>
                        <Dashboards authorizationToken={this.state.authorizationToken}
                                    navigateToCallCenter={this.navigateToCallCenter} reportRoomNights={this.state.reportRoomNights} reportReservations={this.state.reportReservations}/>
                    </div>
                )
            }
        } else {
            div = (
                <div>
                    <LogInForm onSubmit={this.getAccessToken}/>
                </div>
            )
        }

        return (
            div
        );
    }
}

export default App;